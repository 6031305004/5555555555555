import * as React from 'react';

import { NavigationContainer } from '@react-navigation/native';
import { createStackNavigator } from '@react-navigation/stack';
import { createBottomTabNavigator } from '@react-navigation/bottom-tabs';

import { HomeScreen } from "./scr/home"
import { DetailsScreen } from "./scr/detail"
import { FoodsScreen } from "./scr/food"
import { ExercisesScreen } from "./scr/exercise"
import { Exercises2Screen } from "./scr/exercise2"
import { ContrackScreen } from "./scr/contrack"
import { Login } from "./scr/login"
import { Loading } from "./scr/loding"
import Register from './scr/register';


import firebase from 'firebase'
import { firebaseConfig } from "./firebase";

firebase.initializeApp(firebaseConfig)

const HomeStack = createStackNavigator();

function HomeStackScreen() {
  return (
    <HomeStack.Navigator>
      <HomeStack.Screen name="Profile" component={HomeScreen} />
      <HomeStack.Screen name="Contrack" component={ContrackScreen} />
      <HomeStack.Screen name="Details" component={DetailsScreen} />
    </HomeStack.Navigator>
  );
}

const FoodStack = createStackNavigator();

function FoodStackScreen() {
  return (
    <FoodStack.Navigator>
      <FoodStack.Screen name="Foods" component={FoodsScreen} />
      <FoodStack.Screen name="Details" component={DetailsScreen} />
    </FoodStack.Navigator>
  );
}


const ExcercisesStack = createStackNavigator();

function ExerciseStackScreen() {
  return (
    <ExcercisesStack.Navigator>
      <ExcercisesStack.Screen name="Exercise" component={ExercisesScreen} />
      <ExcercisesStack.Screen name="Exercises" component={Exercises2Screen} />
    </ExcercisesStack.Navigator>
  );
}

const TabStack = createBottomTabNavigator();

function TabStackScreen() {
  return (
    <TabStack.Navigator>
      <TabStack.Screen name="Profile" component={HomeStackScreen} />
      <TabStack.Screen name="Food" component={FoodStackScreen} />
      <TabStack.Screen name="Exercise" component={ExerciseStackScreen} />
    </TabStack.Navigator>

  );
}

const AuthStack = createStackNavigator();

export default function App () {
 
    return (
      <NavigationContainer>
        <AuthStack.Navigator headerMode="none">
          <AuthStack.Screen name="Loading" component={Loading} />
          <AuthStack.Screen name="Login" component={Login} />
          <AuthStack.Screen name="Register" component={Register} />
          <AuthStack.Screen name="Home" component={TabStackScreen} />
        </AuthStack.Navigator>
      </NavigationContainer>
    )
  
}