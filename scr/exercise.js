import React, { Component } from 'react'
import { Button, Text, View, ScrollView, StyleSheet } from 'react-native';


export class ExercisesScreen extends Component {
    render() {
        const { navigation } = this.props;
        return (
            <View style={{ flex: 1, justifyContent: 'center', alignItems: 'center' }}>

                <ScrollView>
                    <View style={{justifyContent: 'center'}}>
                    <Text style={{fontSize:30}}>ออกกำลังกาย</Text>
                    </View>
                    <View style={{ marginTop: 30 }} >

                        <Button title="จัดตารางออกกำลังกาย" />

                    </View>
                    <View style={{ marginTop: 30 }}>
                        <Button title="รายการออกกำลัง"
                            onPress={() => navigation.navigate('Exercises')}
                        />

                    </View>

                </ScrollView>
            </View>
        );
    }
}



export default ExercisesScreen
