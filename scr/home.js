import React, {useState, Component } from 'react'
import { Image, Button, Text, View, ScrollView } from 'react-native';
import firebase from 'firebase'

import person from '../image/person.jpg'

export class HomeScreen extends Component {

  

  render() {
    const { navigation } = this.props;



    // var name, email, photoUrl, uid, emailVerified;
    var user = firebase.auth().currentUser;
    var name, email, photoUrl, uid, emailVerified, cal;

    if (user != null) {
      name = user.displayName;
      email = user.email;
      photoUrl = user.photoURL;
      emailVerified = user.emailVerified;
      uid = user.uid;
     
    } 
    if (user != null) {
      user.providerData.forEach(function (profile) {
        console.log("Sign-in provider: " + profile.providerId);
        console.log("  Provider-specific UID: " + profile.uid);
        console.log("  Name: " + profile.displayName);
        console.log("  Email: " + profile.email);
        console.log("  Photo URL: " + profile.photoURL);
      });
    }

    
    return (
      <View style={{ flex: 1, backgroundColor: '#fabd' }}>
        <View style={{ flex: 1, justifyContent: 'center', alignItems: 'center', margin: 'auto' }}>
          <View>
            <View>
              <Image source={person} style={{ marginTop: 10, width: 300, height: 200, borderRadius: 60 }}></Image>
            </View>
            <Text style={{ fontSize: 20 }}>{user.displayName}</Text>
            <Text style={{ fontSize: 10 }}>{user.email}</Text>

          </View>
          <View>
            <Button title='แก้ไขโปรไฟล์'
              onPress={() => navigation.navigate('Details')}
            ></Button>
          </View>
          <ScrollView>
            <View>
              <View style={{ margin: 10, marginTop: 20 }}>
                <View style={{
                  flex: 1,
                  flexDirection: 'row',
                  justifyContent: 'space-around',
                }}>
                  <View style={{ marginRight: 30 }}>
                    <Text>น้ำหนัก</Text>
              <Text>60</Text>
                  </View>
                  <View style={{ marginLeft: 5, marginRight: 5 }}>
                    <Text>ระยะเวลา</Text>
                    <Text>1 เดือน</Text>
                  </View>
                  <View style={{ marginLeft: 30 }}>
                    <Text>เป้าหมาย</Text>
                    <Text>40</Text>
                  </View>
                </View>
              </View>
              <View style={{ margin: 10 }}>
                <View style={{
                  flex: 1,
                  flexDirection: 'row',
                  justifyContent: "center"
                }}>
                  <View style={{ justifyContent: "center", marginRight: 30 }}>
                    <Text>น้ำหนักล่าสุด</Text>
                    <Text>60</Text>
                  </View>
                  <View style={{ marginLeft: 5, marginRight: 5 }}>
                    <Text>เวลาที่เหลือ</Text>
                    <Text>1 เดือน</Text>
                  </View>
                  <View style={{ marginLeft: 30 }}>
                    <Text>ความสำเร็จ</Text>
                    <Text>40</Text>
                  </View>
                </View>
              </View>
            </View>

            <View style={{ marginTop: 50 }}>
              <Button color="green" title="ติดต่อเทรนเนอร์"
                onPress={() => navigation.navigate('Contrack')}
              >

              </Button>
            </View>
            <View style={{ marginTop: 30 }}>
              <Button color="red" title="ออกจากระบบ"
                onPress={() => firebase.auth().signOut()}>
              </Button>
            </View>
          </ScrollView>

        </View>
      </View>
    );
  }
}



export default HomeScreen
