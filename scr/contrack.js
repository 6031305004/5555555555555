import React, { Children, Component } from "react";
import { Button, Text, View, TextInput, ScrollView } from "react-native";
import firebase from "firebase";
import { ChildCare } from "@material-ui/icons";
export class ContrackScreen extends Component {
  state = {
    message: "",
  };

  writeUserData = () => {
    //   var database = firebase.database();
    console.log("Write User Data");
    firebase.database().ref("Food/").push().set({
      message: this.state.message,
    });
  };
  render() {
    return (
      <View style={{ flex: 1, justifyContent: "center", margin: 30 }}>
        <ScrollView>
          <Text style={{ fontSize: 20 }}>พิมอะไรสักอย่างสิ</Text>

          <TextInput
            style={{ backgroundColor: "white", height: 60 }}
            onChangeText={(message) => this.setState({ message })}
            value={this.state.message}
            tyle={{
              borderWidth: 1,
              height: 20,
              backgroundColor: "white",
              marginTop: 50,
            }}
          ></TextInput>
          <View style={{ margin: 50, height: 50 }}>
            <Button
              title="ส่งให้เทรนเนอร์"
              onPress={() => this.writeUserData()}
            />
          </View>
        </ScrollView>
      </View>
    );
  }
}

export default ContrackScreen;
