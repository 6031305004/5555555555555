import React from "react";
import {
    View,
    StyleSheet,
    Button,
    TextInput,
    Text
} from "react-native";
import firebase from 'firebase';


export class Register extends React.Component {
    state = {
        email: "",
        password: "",
        displayName: "",
        // errorMassage: null
    }
    handleRegister = () => {
        const { email, password} = this.state
     
        firebase.auth().createUserWithEmailAndPassword(email, password).then(function (user) {
            // user signed in
        }) .catch(function(error) {
            var errorCode = error.code;
            var errorMessage = error.message;
        
            if (errorCode === 'auth/wrong-password') {
                alert('Wrong password.');
            } else {
                alert(errorMessage);         
            }
            console.log(error);
        });
    }
    render() {

        return (
            <View style={{ flex: 1, backgroundColor: '#faad', }}>
                <View style={{ flex: 1, margin: 50 }}>
                    <View style={{ alignItems: 'center' }}>
                        <Text style={{ fontSize: 20 }}>สมัครสมาชิก</Text>

                    </View>
                   
                    <View style={{ marginTop: 40, borderWidth: 1, borderRadius: 5, backgroundColor: 'white' }}>
                        <TextInput
                            borderless
                            placeholder="อีเมล"
                            onChangeText={email => this.setState({ email })}
                            value={this.state.email}
                            style={{ fontSize: 20 }}
                        ></TextInput>
                    </View>
                    <View style={{ marginTop: 50, borderWidth: 1, borderRadius: 5, backgroundColor: 'white' }}>
                        <TextInput
                        secureTextEntry={true}
                            password
                            borderless
                            placeholder="รหัสผ่าน"
                            onChangeText={password => this.setState({ password })}
                            value={this.state.password}
                            style={{ fontSize: 20 }}
                        />
                    </View>
                    {/* {this.state.errorMassage && <Text style={{ color: "#FF0000" }}>{this.state.errorMassage} </Text>} */}
                    <View style={{ marginTop: 50 }}>
                        <Button title="สมัครสมาชิก :)"
                            onPress={this.handleRegister}

                       
                        />
                    </View>
                </View>
            </View>
        )

    }
}

export default Register;