import React, { Component } from 'react'
import { Text, View, ActivityIndicator } from 'react-native'
import firebase from 'firebase'

export class Loading extends Component {

    componentDidMount(){
        this.checkIfLoggedIn();
    }

    checkIfLoggedIn = () => {
        firebase.auth().onAuthStateChanged(function(user){
            if(user){
                this.props.navigation.navigate('Home');
            }
            else{
                this.props.navigation.navigate('Login');
            }
        }.bind(this)) 
    }

    render() {
        return (
            <View style={{flex : 1 , margin : 100 , marginTop : 250}}>
                <ActivityIndicator size="large"/>
            </View>
        )
    }
}

export default Loading;