import React, { Component } from 'react'
import { Button, Text, View, ScrollView, StyleSheet } from 'react-native';
import CardFood from '../component/cardfood';
import firebase from 'firebase'
import { call } from 'react-native-reanimated';

export class FoodsScreen extends Component {

    constructor(props) {
        super(props);
        this.state = {
            Food: []
        }
    }
    componentDidMount() {
        fetch("https://train-me-food.firebaseio.com/Food.json")
            .then(res => res.json())
            .then((data) => {
                this.setState({
                    Food: data
                })
            })
    }

    render() {



        const { navigation } = this.props;
        return (
            <View style={{ flex: 1, justifyContent: 'center', alignItems: 'center' }}>

                <ScrollView>
                    <View style={{ justifyContent: 'center', alignItems: 'center' }}>
                        <Text style={styles.textsize}>อาหารเช้า</Text>

                        {Object.values(this.state.Food).map((item) => {
                            return <CardFood
                                title={item.name}
                                img={item.pic}
                                cal={item.cal}
                            >

                            </CardFood>
                        })
                        }

                    </View>
                    {/* <View style={{ justifyContent: 'center', alignItems: 'center' }}>
                        <Text style={styles.textsize}>อาหารกลางวัน</Text>
                        <View style={styles.gallery} >
                            <CardFood
                                img={{ uri: 'https://img.wongnai.com/p/1920x0/2020/01/27/0b6131adac0549a19fdcea5ae5dceda0.jpg' }}
                                title={"BraekFast"}
                                cal={120}
                            ></CardFood>
                            <CardFood
                                img={{ uri: 'data:image/jpeg;base64,/9j/4AAQSkZJRgABAQAAAQABAAD/2wCEAAkGBxAQEBANDw8QDxANDQ0PDQ8ODw8NDg0OFREWFhURFhMYHCgjGBolGxMVIz0hJSorLi4uGB8zOD8sNyguLisBCgoKDQ0OFQ0NDisZFR0rKzcrNy0rKys3KysrKy0tLSsrKy0rKysrKysrLSsrKysrKysrKysrKysrKysrKysrK//AABEIALQBGQMBIgACEQEDEQH/xAAbAAEAAgMBAQAAAAAAAAAAAAAAAgQBAwUGB//EAEIQAAIBAwAFCAUICQUBAAAAAAABAgMEEQUSITFRE1JhcYGRocEGIjJBsVNigpKistHhFCQzVGNyk9LwNUJDg8Ij/8QAFgEBAQEAAAAAAAAAAAAAAAAAAAEC/8QAFhEBAQEAAAAAAAAAAAAAAAAAAAER/9oADAMBAAIRAxEAPwD7iAAAAAAAAAAAAAAAAAAAIuaW9pdph1Y8e5NgTBr5VdPcxyy/xMDYCHKr/Exyq4/ECYIqa4rvJAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAABT0vfq3ozryTahqZxtxrSUdZ9CznqRG3rqpBVIzU4ySalBqUJdTWwo+m0saPumt6pZXZJFDQWj6M6Maqi6c5a2vOhOpQlNxk1mTg1rbvfko9CgVYUJLdWqP+dUpr7qb7yaVXnU3/ANck/vhG8GnNThB/SlHyZjXqcyn/AFZf2AbzBp1qvMp/1J/2DNT+GvrS/ADcY3bm11GtKXvkvoxx8WzONq2t9b/ADda13Jyi9urj1luefd1osle1W9cPzLBFAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAcL06/wBNvOi2m+7aVPROrm3S4TqL7bfmXfTZZ0bfZ/c7h90GzynoTpCdOgoThJuVSVRNNYUZbUtpYj3KJFGjdOSysLoaz5iVefFdwwXjBQdepx8ER5epx8EB0TBR/SKnHwIyuqi5van+IwdBmFvOdG/qfJxa4qWDbSv0360XHxQHTt98uteZvNFs976TeRQAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAFfSMc0aq35pT2Pc/VZ5jReiqKiuTTpZ2tU36nZB5S7Ej1V0s05rjCa8Gee0RL1UWIuU7Nx3SWODi0+/PkZlbT+b9Z/gWomxAc12tTh9pEJWtTHsZ6Mwz4s6oGjlO3q42U31KVNY+0HaVX/ALe+UVjuydYyho5ENH1ffyaz8+T/APJmVrOOVKpvx7EFFrtk3nuR1yldPaUS0E241W23+sVEstt4WF5HTOZ6P/spS51e4a6uVkvI6ZlQAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAEaizFrimvA8voiWztPVHk9GrDlHg2vEsSu7A2I1UzagBkAAZRgkgBSre12ouvccvSFXUhUnzKdSX1Yt+QgsejO20oS58Nf6zcvM6hS0LS1LahDm0KS+wi6RQAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA8tSjq1qq/i1O7J6k83cxxc1FxcX3xTLB1KRtRoo7jegiWDOAiSAjgyZaMARm9hwtPy/V6/wA6lOC65LVXxO5Vew4emVmEIfKXVpDsdeDfgmWD0lOOEo8El3IkAZUAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAPP6Vji5zzqcH3OS8j0BxNPLFWjLjGon2OLXxZYLFu9hYiVbZ7C1EVGxEkYRkKMwzIA013sOPerNeyhzrtyfVChVl8cHWrs51KOteUF7qdvdVOqTlSgvByCO8ACKAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAHK0/D1acuFXHY4vzSOqUNOR/8AhJ81wl1JSWfDIFa0ewuxOdZSOjA1UbUSMIkjKhFkyMgKlcraLjm4rS5lGhTXQ25zl8YFisafR7aq9X5S7qpdVNRpfGnIqOsACKAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAGi+o69KpT59Oce1xaN4A8toq51oRlzoxfejtUJnibe5qULitbVIOKjXq8jNZ1JUtduG3dnDxjoPRWl6VHeRIowu0S/S1xIq42aqsypO9RUr33BNlE726UU5N7k2y3oClq21FPY5U1Ul/PUevLxkzyWmL2DUKUprWuKioxprOu3N6qfez01npZTquglDEZzpxcZ1JP1HOLUk4JReab2ZezbzdZR1wAQAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAADDMkJSA8r6USjGTjrxU5uM4Qcoqc04tSxF7/AGGarWlGcVKE8PC9nGF9H3F70h0TG4q21wnq1bSo5RezE6UvapvPx6+OSddVdmpSo1OicnTn91osRTVvUX/L3x/M2xjPnL6v5mt3VZbJWFRdNOtCa7kycb1/udx3zfmUQq2dWe6tqZ4Qz5ohDQkNZcpOtWfCU3GC7I+ZYlf1l+zsasn86agu9nJ0zf3zhJKhTptpLk41VUqyT2NLGIp9LawB5T0VpSvNNyrZcqVC4rTi85jGnDWVPHRlRPo2g6C/Srqq4x1nOSjNRUdeKqzTxhZWNVRec5cG1se3m+itjTteUnGnCnKs/Ypr1KUPdFP3vi8Jbkt2X1rS2oQqzuFSjytSTlKo0nLLzu4bHjZvW/Jmq7wK0LnJtjVA2AwmZAAAAAAAAAAAAAAAAAAAAAAAAAAAAAADNckbCLQFKrD1t3BGXbo3OO0zqgVuQXSuptDkOmX1mWlEzgCnKzzvbf0pfiRWjIL3dyL+DIFSNhBG2NrE3JEgNcaKJqCJAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAYAAjgYMAAZAABAASRkAAAAAAAAAAAAAAAAAAAAP//Z' }}
                                title={"น้ำปล่าว"}
                                cal={0}
                            ></CardFood>

                        </View>
                    </View>
                    <View style={{ justifyContent: 'center', alignItems: 'center' }}>
                        <Text style={styles.textsize}>อาหารเย็น</Text>
                        <View style={styles.gallery} >
                            <CardFood
                                img={{ uri: 'https://img.wongnai.com/p/1920x0/2020/01/27/0b6131adac0549a19fdcea5ae5dceda0.jpg' }}
                                title={"BraekFast"}
                                cal={120}
                            ></CardFood>
                            <CardFood
                                img={{ uri: 'data:image/jpeg;base64,/9j/4AAQSkZJRgABAQAAAQABAAD/2wCEAAkGBxAQEBANDw8QDxANDQ0PDQ8ODw8NDg0OFREWFhURFhMYHCgjGBolGxMVIz0hJSorLi4uGB8zOD8sNyguLisBCgoKDQ0OFQ0NDisZFR0rKzcrNy0rKys3KysrKy0tLSsrKy0rKysrKysrLSsrKysrKysrKysrKysrKysrKysrK//AABEIALQBGQMBIgACEQEDEQH/xAAbAAEAAgMBAQAAAAAAAAAAAAAAAgQBAwUGB//EAEIQAAIBAwAFCAUICQUBAAAAAAABAgMEEQUSITFRE1JhcYGRocEGIjJBsVNigpKistHhFCQzVGNyk9LwNUJDg8Ij/8QAFgEBAQEAAAAAAAAAAAAAAAAAAAEC/8QAFhEBAQEAAAAAAAAAAAAAAAAAAAER/9oADAMBAAIRAxEAPwD7iAAAAAAAAAAAAAAAAAAAIuaW9pdph1Y8e5NgTBr5VdPcxyy/xMDYCHKr/Exyq4/ECYIqa4rvJAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAABT0vfq3ozryTahqZxtxrSUdZ9CznqRG3rqpBVIzU4ySalBqUJdTWwo+m0saPumt6pZXZJFDQWj6M6Maqi6c5a2vOhOpQlNxk1mTg1rbvfko9CgVYUJLdWqP+dUpr7qb7yaVXnU3/ANck/vhG8GnNThB/SlHyZjXqcyn/AFZf2AbzBp1qvMp/1J/2DNT+GvrS/ADcY3bm11GtKXvkvoxx8WzONq2t9b/ADda13Jyi9urj1luefd1osle1W9cPzLBFAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAcL06/wBNvOi2m+7aVPROrm3S4TqL7bfmXfTZZ0bfZ/c7h90GzynoTpCdOgoThJuVSVRNNYUZbUtpYj3KJFGjdOSysLoaz5iVefFdwwXjBQdepx8ER5epx8EB0TBR/SKnHwIyuqi5van+IwdBmFvOdG/qfJxa4qWDbSv0360XHxQHTt98uteZvNFs976TeRQAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAFfSMc0aq35pT2Pc/VZ5jReiqKiuTTpZ2tU36nZB5S7Ej1V0s05rjCa8Gee0RL1UWIuU7Nx3SWODi0+/PkZlbT+b9Z/gWomxAc12tTh9pEJWtTHsZ6Mwz4s6oGjlO3q42U31KVNY+0HaVX/ALe+UVjuydYyho5ENH1ffyaz8+T/APJmVrOOVKpvx7EFFrtk3nuR1yldPaUS0E241W23+sVEstt4WF5HTOZ6P/spS51e4a6uVkvI6ZlQAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAEaizFrimvA8voiWztPVHk9GrDlHg2vEsSu7A2I1UzagBkAAZRgkgBSre12ouvccvSFXUhUnzKdSX1Yt+QgsejO20oS58Nf6zcvM6hS0LS1LahDm0KS+wi6RQAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA8tSjq1qq/i1O7J6k83cxxc1FxcX3xTLB1KRtRoo7jegiWDOAiSAjgyZaMARm9hwtPy/V6/wA6lOC65LVXxO5Vew4emVmEIfKXVpDsdeDfgmWD0lOOEo8El3IkAZUAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAPP6Vji5zzqcH3OS8j0BxNPLFWjLjGon2OLXxZYLFu9hYiVbZ7C1EVGxEkYRkKMwzIA013sOPerNeyhzrtyfVChVl8cHWrs51KOteUF7qdvdVOqTlSgvByCO8ACKAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAHK0/D1acuFXHY4vzSOqUNOR/8AhJ81wl1JSWfDIFa0ewuxOdZSOjA1UbUSMIkjKhFkyMgKlcraLjm4rS5lGhTXQ25zl8YFisafR7aq9X5S7qpdVNRpfGnIqOsACKAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAGi+o69KpT59Oce1xaN4A8toq51oRlzoxfejtUJnibe5qULitbVIOKjXq8jNZ1JUtduG3dnDxjoPRWl6VHeRIowu0S/S1xIq42aqsypO9RUr33BNlE726UU5N7k2y3oClq21FPY5U1Ul/PUevLxkzyWmL2DUKUprWuKioxprOu3N6qfez01npZTquglDEZzpxcZ1JP1HOLUk4JReab2ZezbzdZR1wAQAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAADDMkJSA8r6USjGTjrxU5uM4Qcoqc04tSxF7/AGGarWlGcVKE8PC9nGF9H3F70h0TG4q21wnq1bSo5RezE6UvapvPx6+OSddVdmpSo1OicnTn91osRTVvUX/L3x/M2xjPnL6v5mt3VZbJWFRdNOtCa7kycb1/udx3zfmUQq2dWe6tqZ4Qz5ohDQkNZcpOtWfCU3GC7I+ZYlf1l+zsasn86agu9nJ0zf3zhJKhTptpLk41VUqyT2NLGIp9LawB5T0VpSvNNyrZcqVC4rTi85jGnDWVPHRlRPo2g6C/Srqq4x1nOSjNRUdeKqzTxhZWNVRec5cG1se3m+itjTteUnGnCnKs/Ypr1KUPdFP3vi8Jbkt2X1rS2oQqzuFSjytSTlKo0nLLzu4bHjZvW/Jmq7wK0LnJtjVA2AwmZAAAAAAAAAAAAAAAAAAAAAAAAAAAAAADNckbCLQFKrD1t3BGXbo3OO0zqgVuQXSuptDkOmX1mWlEzgCnKzzvbf0pfiRWjIL3dyL+DIFSNhBG2NrE3JEgNcaKJqCJAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAYAAjgYMAAZAABAASRkAAAAAAAAAAAAAAAAAAAAP//Z' }}
                                title={"น้ำปล่าว"}
                                cal={0}
                            ></CardFood>

                        </View>
                    </View> */}
                </ScrollView>
            </View>
        );
    }
}

const styles = StyleSheet.create({
    gallery: {
        flex: 1,
        flexWrap: 'wrap',
        justifyContent: "space-around",
        flexDirection: 'row',


    },
    item: {
        width: 30,
        height: 30
    },
    textsize: {
        fontSize: 30
    }


})

export default FoodsScreen

