import React from "react";
import {
    View,
    StyleSheet,
    ImageBackground,
    Dimensions,
    StatusBar,
    KeyboardAvoidingView,
    Button,
    TextInput
    , Text
} from "react-native";
import firebase from 'firebase';

export class Login extends React.Component {
    state = {
        email: "",
        password: "",
        // errorMassage: null
    }

    handleLogin = () => {
        const { email, password } = this.state
     
        firebase.auth().signInWithEmailAndPassword(email, password).then(function (user) {
            // user signed in
        }).catch(function (error) {
            var errorCode = error.code;
            // var errorMessage = error.message;

            if (errorCode === 'auth/wrong-password') {
                alert('หรัสผ่านวื้ดดดด ;P');
            } else {
                alert('กรุณากรอก ID ให้ถูกต้อง');
            }
            console.log(error);
        });
    }
    render() {
        const { navigation } = this.props;

        return (
            <View style={{ flex: 1, backgroundColor: '#faad', }}>
                <View style={{ flex: 1, margin: 50 }}>
                    <View style={{ alignItems: 'center' }}>
                        <Text style={{ fontSize: 20 }}>Login Page</Text>

                    </View>
                    <View style={{ marginTop: 40, borderWidth: 1, borderRadius: 5, backgroundColor: 'white' }}>
                        <TextInput
                            borderless
                            placeholder="อีเมล"
                            onChangeText={email => this.setState({ email })}
                            value={this.state.email}
                            style={{ fontSize: 20 }}
                        ></TextInput>
                    </View>
                    <View style={{ marginTop: 50, borderWidth: 1, borderRadius: 5, backgroundColor: 'white' }}>
                        <TextInput
                            secureTextEntry={true}
                            borderless
                            placeholder="รหัสผ่าน"
                            onChangeText={password => this.setState({ password })}
                            value={this.state.password}
                            style={{ fontSize: 20 }}
                        />
                    </View>

                    {/* {this.state.errorMassage && <Text style={{ color: "#FF0000" }}>{this.state.errorMassage} </Text>} */}
                    <View style={{ marginTop: 50 }}>
                        <Button title="Login"
                            onPress={this.handleLogin}

                        // onPress={() => navigation.navigate('Home')}
                        /><br></br>
                         <Button style={{marginTop:10 }}
                         title="ลงทะเบียนเข้าสู่ระบบ"
                            onPress={() =>navigation.navigate('Register')}

                        // onPress={() => navigation.navigate('Home')}
                        />
                    </View>
                </View>
            </View>
        );
    }
}

export default Login;