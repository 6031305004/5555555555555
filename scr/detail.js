import React, { Component } from "react";
import { Button, Text, View, TextInput } from "react-native";
import firebase from "firebase";

export class DetailsScreen extends Component {
  state = {
    displayName: "",
  };

  updateUser = () => {
    console.log(this.state.displayName);
    var user = firebase.auth().currentUser;
    user.updateProfile({
      displayName: this.state.displayName,
    });
  };

  render() {
    return (
      <View style={{ flex: 1, justifyContent: "center", alignItems: "center" }}>
        <TextInput
          placeholder="ชื่อ"
          onChangeText={(displayName) => this.setState({ displayName })}
          value={this.state.displayName}
        ></TextInput>

        <Button title="update1" onPress={() => this.updateUser()} />
      </View>
    );
  }
}

export default DetailsScreen;
