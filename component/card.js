import React, { Component } from 'react'
import { View, StyleSheet, Image, Text } from 'react-native'

export class Card extends Component {
    render() {
        return (
            <View>
                <View style={{ flex: 1, flexDirection: 'row', marginTop: 10, borderColor: 'gray', borderWidth: 0.5, borderRadius: 4, width: 350, backgroundColor: 'white' }}>

                    <View style={{ margin: 20, flexDirection: 'column' }}>
                        <View>
                            <Text style={{ fontSize: 20 }} >
                                {this.props.title}
                            </Text>
                            <Text >
                                เผาผลาน {this.props.cal} Cal
                        </Text>
                        </View>
                        <View>
                            <Text>
                                ประโยชน์ : {this.props.advantage}
                            </Text>
                            
                            <Text>
                                ระยะเวลา {this.props.time} 
                            </Text>

                        </View>
                        <View>
                            <Text>
                                จำนวน/เซ็ท {this.props.amount}
                            </Text>
                            
                           

                        </View>
                    </View>
                </View>
            </View>
        )
    }
}

const styles = StyleSheet.create({
    container: {
        backgroundColor: '#fff'
    }
})

export default Card
