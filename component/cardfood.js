import React, { Component } from 'react'
import { View, StyleSheet, Image, Text } from 'react-native'

export class CardFood extends Component {
    render() {
        return (
            <View >
                <View style={{ flex: 1, flexDirection: 'row', marginTop: 10, borderColor: 'gray', borderWidth: 0.5 , borderRadius:4,width:350, backgroundColor:'white'}}>
                    <View>
                        <Image source={this.props.img}
                            style={{ width: 100, height: 100, margin: 10 }} />
                    </View>
                    <View style={{ marginRight: 10, marginTop: 10 }}>
                        <Text style={{fontSize:20}} >
                            {this.props.title}
                        </Text>
                        <Text >
                            พลังงาน {this.props.cal} Cal
                        </Text>
                       
                    </View>
                </View>
            </View>
        )
    }
}

const styles = StyleSheet.create({
    
})

export default CardFood
